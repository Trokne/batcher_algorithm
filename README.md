# Сортировка Бэтчера

Реализация алгоритма сортировки со слиянием Бэтчера.

## Установка (Windows / VS 2019)

1. Установить пакет [HPC Pack 2008 SDK SP2](https://www.microsoft.com/en-us/download/details.aspx?id=2800)
2. Склонировать репозиторий
3. Открыть проект с помощью [Visual Studio 2019 Community](https://visualstudio.microsoft.com/ru/vs/).
4. Во вкладке Debug - BatcherSort Properties - VC++ Directories в поле Include Directories указать:

`C:\Program Files\Microsoft HPC Pack 2008 SDK\Include`

3. Во вкладке Debug - BatcherSort Properties - VC++ Directories в поле Library Directories указать:

`C:\Program Files\Microsoft HPC Pack 2008 SDK\Lib\amd64`

4. Во вкладке Debug - BatcherSort Properties - Linker - Input в поле Additional Dependencies указать:

`msmpi.lib`

5. В меню Debugging в поле Command указать:

`C:\Program Files\Microsoft HPC Pack 2008 SDK\Bin\mpiexec.exe`

6. В меню Debugging в поле Command Arguments указать (4 указывает на число процессов):

`-n 4 $(TargetPath)`

## Дополнительно

В качестве тестовых примеров в директории BatcherSort расположены несколько файлов:

- input.txt - матрица размером 500 000 элементов
- input1.txt - матрица размером 32 элемента
- input1.txt - матрица размером 1000 элементов

Для того, чтобы воспользоваться файлом, необходимо в файле `FileArrayService.cpp` заменить путь в методе `read_array`.
