#include "MpiTimer.h"


MpiTimer::MpiTimer()
{
	start_time = 0;
	end_time = 0;
}


MpiTimer::~MpiTimer()
{
}

void MpiTimer::startTimer()
{
	start_time = MPI_Wtime();
}

void MpiTimer::stopTimer()
{
	end_time = MPI_Wtime();
}

double MpiTimer::getTime()
{
	return end_time - start_time;
}
