#include "FileArrayService.h"


FileArrayService::FileArrayService(MpiUtils* mpi_utils)
{
	mpi = mpi_utils;
}


FileArrayService::~FileArrayService()
{
}

int FileArrayService::read_array(vector<int>& array_from_file) {
	ifstream file("input.txt");
	int n;
	file >> n;

	auto array_offset = n % mpi->get_process_size();
	array_from_file.resize(n + array_offset);

	for (int i = 0; i < n; i++) {
		file >> array_from_file[i];
	}

	for (int i = 0; i < array_offset; i++) {
		array_from_file[n + i] = INT_MAX;
	}
	return array_offset;
}

void FileArrayService::write_array(const vector<int>& arr, ostream& out) {
	for (size_t i = 0; i < arr.size(); i++) {
		out << arr[i];
		if (i != arr.size() - 1) {
			out << " ";
		}
	}
	out << endl;
}

void FileArrayService::write_array_to_file(const vector<int>& arr) {
	ofstream file("output.txt");
	write_array(arr, file);
}

void FileArrayService::write_cout(const vector<int>& arr) {
	stringstream ss;
	write_array(arr, ss);
	cout << "[" << mpi->get_process_rank() << "]:" << ss.str();
	cout.flush();
}

void FileArrayService::sync_write_cout(const vector<int>& arr, const string& title) {
	int process_rank = mpi->get_process_rank();
	int process_size = mpi->get_process_size();

	int sync = 0;

	if (process_rank > 0)
		MPI_Recv(
			&sync, 1, MPI_INT, process_rank - 1,
			0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	else {
		cout << title << endl;
		cout.flush();
	}

	stringstream ss;
	write_array(arr, ss);
	cout << "[" << process_rank << "]:" << ss.str();
	cout.flush();

	if (process_rank != process_size - 1)
		MPI_Send(
			&sync, 1, MPI_INT, process_rank + 1,
			0, MPI_COMM_WORLD);
	else {
		cout << endl;
		cout.flush();
	}
}
