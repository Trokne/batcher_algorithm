#include "BatcherSort.h"



BatcherSort::BatcherSort(MpiUtils* mpiUtils)
{
	mpi = mpiUtils;
}


BatcherSort::~BatcherSort()
{
}

void BatcherSort::run(vector<int> work_arr, int block_size) {
	auto process_rank = mpi->get_process_rank();
	// sort process part of array
	sort(work_arr.begin(), work_arr.end());

	// create merge lines
	create_merge_lines();

	vector<int> other_vec(block_size);
	vector<int> temp(block_size);

	for (size_t comp_i = 0; comp_i < comparators.size(); comp_i++) {
		auto& comp = comparators[comp_i];

		if (process_rank == 0)
			cout << comp.first << " " << comp.second << "\n";

		if (process_rank == comp.first || process_rank == comp.second)
		{
			int other = process_rank == comp.first ? comp.second : comp.first;

			MPI_Sendrecv(
				work_arr.data(), block_size, MPI_INT, other, comp_i,
				other_vec.data(), block_size, MPI_INT, other, comp_i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

			if (process_rank == comp.first) {
				for (size_t i = 0, j = 0, t_i = 0; t_i < temp.size(); t_i++) {
					if (i < work_arr.size() && (j >= other_vec.size() || work_arr[i] < other_vec[j])) {
						temp[t_i] = work_arr[i];
						i++;
					}
					else {
						temp[t_i] = other_vec[j];
						j++;
					}
				}
			}
			else
			{
				for (int i = (int)work_arr.size() - 1, j = (int)other_vec.size() - 1, t_i = (int)temp.size() - 1; t_i >= 0; t_i--) {
					if (i >= 0 && (j < 0 || work_arr[i] > other_vec[j])) {
						temp[t_i] = work_arr[i];
						i--;
					}
					else {
						temp[t_i] = other_vec[j];
						j--;
					}
				}
			}

			work_arr.swap(temp);
		}
	}

	//sync_write_cout(work_arr, "Scattered result");
}

void BatcherSort::merge_lines(const vector<int>& procs_up, const vector<int>& procs_down)
{
	size_t proc_count = procs_up.size() + procs_down.size();
	if (proc_count == 1) {
		return;
	}
	else if (proc_count == 2) {
		comparators.emplace_back(procs_up.front(), procs_down.front());
		return;
	}

	vector<int> procs_up_even;
	vector<int> procs_up_odd;

	for (size_t i = 0; i < procs_up.size(); i++) {
		if (i % 2) {
			procs_up_even.push_back(procs_up[i]);
		}
		else {
			procs_up_odd.push_back(procs_up[i]);
		}
	}

	vector<int> procs_down_even;
	vector<int> procs_down_odd;

	for (size_t i = 0; i < procs_down.size(); i++) {
		if (i % 2) {
			procs_down_even.push_back(procs_down[i]);
		}
		else {
			procs_down_odd.push_back(procs_down[i]);
		}
	}

	merge_lines(procs_up_odd, procs_down_odd);
	merge_lines(procs_up_even, procs_down_even);

	vector<int> result = procs_up;
	result.insert(result.end(), procs_down.begin(), procs_down.end());

	for (int i = 1; i + 1 < result.size(); i += 2) {
		comparators.emplace_back(result[i], result[i + 1]);
	}
}

void BatcherSort::split_lines(const vector<int> & procs)
{
	if (procs.size() == 1)
		return;

	std::size_t const half_size = procs.size() / 2;
	std::vector<int> procs_up(procs.begin(), procs.begin() + half_size);
	std::vector<int> procs_down(procs.begin() + half_size, procs.end());

	split_lines(procs_up);
	split_lines(procs_down);
	merge_lines(procs_up, procs_down);
}

void BatcherSort::create_merge_lines() {
	std::vector<int> procs;

	procs.resize(mpi->get_process_size());
	for (int i = 0; i < mpi->get_process_size(); i++)
		procs[i] = i;

	split_lines(procs);
}
