#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "mpiutils.h"

#pragma once

using namespace std;

class FileArrayService
{
public:
	FileArrayService(MpiUtils* mpi_utils);
	~FileArrayService();
	int read_array(vector<int>& array_from_file);
	void write_array(const vector<int>& arr, ostream& out);
	void write_array_to_file(const vector<int>& arr);
	void write_cout(const vector<int>& arr);
	void sync_write_cout(const vector<int>& arr, const string& title);
private:
	MpiUtils* mpi;
};

