#include <iostream>
#include <vector>
#include <cstdlib>
#include <fstream>

#include <chrono>
#include <thread>

#include "mpiutils.h"
#include "MpiTimer.h"
#include "Source.h"
#include "FileArrayService.h"
#include "BatcherSort.h"

using namespace std;

MpiUtils* mpi;
FileArrayService* file_array_service;
BatcherSort* batcher;
MpiTimer* mpi_timer;


void initializeDependencies() {
	mpi = new MpiUtils();
	batcher = new BatcherSort(mpi);
	file_array_service = new FileArrayService(mpi);
	mpi_timer = new MpiTimer();
}

void print_start_information(int arr_size, int block_size) {
	if (mpi->get_process_rank() == 0)
	{
		cout << "Count of processes: " << mpi->get_process_size() << "\n";
		cout << "Full array size: " << arr_size << "\n";
		cout << "Block array size: " << block_size << "\n";
	}
}

void print_final_information() {
	cout << "[" << mpi->get_process_rank() << "]:" << "\n";
	cout << "MPI time : " << mpi_timer->getTime() << "s.\n";
}

int main(int argc, char** argv) {
	vector<int> arr;
	vector<int> work_arr;
	int array_offset = 0;
	int block_size = 0;
		
	initializeDependencies();
	mpi->initializeMpi(&argc, &argv);

    if (mpi->get_process_rank() == 0) {
        array_offset = file_array_service->read_array(arr);
    }

    int arr_size = static_cast<int>(arr.size());
    mpi->send_value_to_other_processes(&arr_size, 1, MPI_INT, 0);
    block_size = arr_size / mpi->get_process_size();
	print_start_information(arr_size, block_size);

    work_arr.resize(block_size);

    MPI_Scatter(
        arr.data(), block_size, MPI_INT,
        work_arr.data(), block_size, MPI_INT, 0, MPI_COMM_WORLD);

    //sync_write_cout(work_arr, "Scattered input");
    mpi->wait_all();
	mpi_timer->startTimer();
	batcher->run(work_arr, block_size);

	MPI_Gather(
		work_arr.data(), block_size, MPI_INT,
		arr.data(), block_size, MPI_INT,
		0, MPI_COMM_WORLD);

    if (mpi->get_process_rank() == 0) {
        arr.resize(arr.size() - array_offset);
       // cout << "Gathered input" << endl;
       // write_array(arr, cout);
        file_array_service->write_array_to_file(arr);
    }

	mpi_timer->stopTimer();
	print_final_information();
    MPI_Finalize();
    return 0;
}
