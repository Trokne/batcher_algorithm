#include "mpiutils.h"


MpiUtils::MpiUtils() {
	process_size = process_rank = 0;
}

MpiUtils::~MpiUtils() {}

void MpiUtils::initializeMpi(int* argc, char*** argv) {
	MPI_Init(argc, argv);
	MPI_Comm_size(MPI_COMM_WORLD, &process_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &process_rank);
}

void MpiUtils::send_value_to_other_processes(void* data, int count, MPI_Datatype type, int from_process_number)
{
	MPI_Bcast(data, count, type, from_process_number, MPI_COMM_WORLD);
}

int MpiUtils::get_process_size()
{
	return process_size;
}

int MpiUtils::get_process_rank()
{
	return process_rank;
}

void MpiUtils::wait_all()
{
	MPI_Barrier(MPI_COMM_WORLD);
}
