#include "mpi.h"

#pragma once
class MpiUtils
{
public:
	MpiUtils();
	~MpiUtils();
	void initializeMpi(int* argc, char*** argv);
	void send_value_to_other_processes(void* data, int count, MPI_Datatype type, int from_process_number);
	int get_process_size();
	int get_process_rank();
	void wait_all();

private:
	int process_size;		//w_size
	int process_rank;		//w_count
};

