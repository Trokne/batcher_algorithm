#include "arrayutils.h"


ArrayUtils::ArrayUtils()
{
}


ArrayUtils::~ArrayUtils()
{
}

void ArrayUtils::read_array(vector<int>& arr) {
	ifstream file("input.txt");
	int n;
	file >> n;
	arr_offset = n % w_size;

	arr.resize(n + arr_offset);

	for (int i = 0; i < n; i++) {
		file >> arr[i];
	}

	for (int i = 0; i < arr_offset; i++) {
		arr[n + i] = INT_MAX;
	}
}