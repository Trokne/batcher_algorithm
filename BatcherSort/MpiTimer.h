#include "mpi.h"

#pragma once
class MpiTimer
{
public:
	MpiTimer();
	~MpiTimer();
	void startTimer();
	void stopTimer();
	double getTime();
private:
	double start_time;
	double end_time;
};

