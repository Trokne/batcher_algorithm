#pragma once
#include "mpiutils.h"
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class BatcherSort
{
public:
	BatcherSort(MpiUtils* mpiUtils);
	~BatcherSort();
	void run(vector<int> work_arr, int block_size);
private:
	MpiUtils* mpi;
	vector<pair<int, int>> comparators;
	void merge_lines(const vector<int>& procs_up, const vector<int>& procs_down);
	void split_lines(const vector<int>& procs);
	void create_merge_lines();
};

